﻿namespace GameRenderer
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using GameModel;

    /// <summary>
    /// Gamerenderer class with the needed geometry for rendering.
    /// </summary>
    public class DungeonRenderer
    {
        private IGameModel model;

        private Rect firstAisle;
        private Rect secondAisle;
        private Rect thirdAisle;

        private Dictionary<string, Brush> brushes = new Dictionary<string, Brush>();

        private Point firstRightFloor1;
        private Point firstRightFloor2;
        private Point firstRightFloor3;

        private Point firstLeftFloor1;
        private Point firstLeftFloor2;
        private Point firstLeftFloor3;

        private Rect firstLeftWall;
        private Rect firstRightWall;

        private Point firstRightTop1;
        private Point firstRightTop2;
        private Point firstRightTop3;

        private Point firstLeftTop1;
        private Point firstLeftTop2;
        private Point firstLeftTop3;

        private Point secondRightFloor1;
        private Point secondRightFloor2;
        private Point secondRightFloor3;

        private Point secondLeftFloor1;
        private Point secondLeftFloor2;
        private Point secondLeftFloor3;

        private Rect secondRightWall;
        private Rect secondLeftWall;

        private Point secondLeftTop1;
        private Point secondLeftTop2;
        private Point secondLeftTop3;

        private Point secondRightTop1;
        private Point secondRightTop2;
        private Point secondRightTop3;

        private Pen wallPen = new Pen(Brushes.Black, 2);

        /// <summary>
        /// Initializes a new instance of the <see cref="DungeonRenderer"/> class.
        /// </summary>
        /// <param name="model">Model type parameter.</param>
        public DungeonRenderer(IGameModel model)
        {
            this.model = model;

            this.firstAisle = new Rect(0, 0, this.model.GameWidth, this.model.GameHeight);
            this.secondAisle = new Rect(this.model.GameWidth / 4, this.model.GameHeight / 4, this.model.GameWidth / 2, this.model.GameHeight / 2);
            this.thirdAisle = new Rect(3 * this.model.GameWidth / 8, 3 * this.model.GameHeight / 8, this.model.GameWidth / 4, this.model.GameHeight / 4);

            this.firstLeftWall = new Rect(0, 2 * this.model.GameHeight / 8, 2 * this.model.GameWidth / 8, this.model.GameHeight / 2);
            this.firstRightWall = new Rect(6 * this.model.GameWidth / 8, 2 * this.model.GameHeight / 8, 2 * this.model.GameWidth / 8, this.model.GameHeight / 2);

            this.firstRightFloor1 = new Point(this.model.GameWidth, 6 * this.model.GameHeight / 8);
            this.firstRightFloor2 = new Point(6 * this.model.GameWidth / 8, 6 * this.model.GameHeight / 8);
            this.firstRightFloor3 = new Point(this.model.GameWidth, this.model.GameHeight);

            this.firstLeftFloor1 = new Point(0, 6 * this.model.GameHeight / 8);
            this.firstLeftFloor2 = new Point(2 * this.model.GameWidth / 8, 6 * this.model.GameHeight / 8);
            this.firstLeftFloor3 = new Point(0, this.model.GameHeight);

            this.firstRightTop1 = this.firstRightWall.TopRight;
            this.firstRightTop2 = this.firstRightWall.TopLeft;
            this.firstRightTop3 = new Point(this.model.GameWidth, 0);

            this.firstLeftTop1 = this.firstLeftWall.TopLeft;
            this.firstLeftTop2 = this.firstLeftWall.TopRight;
            this.firstLeftTop3 = new Point(0, 0);

            this.secondLeftWall = new Rect(2 * this.model.GameWidth / 8, 3 * this.model.GameHeight / 8, this.model.GameWidth / 8, this.model.GameHeight / 4);
            this.secondRightWall = new Rect(5 * this.model.GameWidth / 8, 3 * this.model.GameHeight / 8, this.model.GameWidth / 8, this.model.GameHeight / 4);

            this.secondRightFloor1 = this.secondRightWall.BottomRight;
            this.secondRightFloor2 = this.secondRightWall.BottomLeft;
            this.secondRightFloor3 = this.firstRightFloor2;

            this.secondLeftFloor1 = this.secondLeftWall.BottomLeft;
            this.secondLeftFloor2 = this.secondLeftWall.BottomRight;
            this.secondLeftFloor3 = this.firstLeftFloor2;

            this.secondRightTop1 = this.secondRightWall.TopRight;
            this.secondRightTop2 = this.secondRightWall.TopLeft;
            this.secondRightTop3 = this.firstRightTop2;

            this.secondLeftTop1 = this.secondLeftWall.TopLeft;
            this.secondLeftTop2 = this.secondLeftWall.TopRight;
            this.secondLeftTop3 = this.firstLeftTop2;
        }

        private Brush WallBrush
        {
            get { return this.GetBrush("GameRenderer.Images.wall.bmp", false); }
        }

        private Brush FloorBrush
        {
            get { return this.GetBrush("GameRenderer.Images.floor3.bmp", false); }
        }

        private Brush TopBrush
        {
            get { return this.GetBrush("GameRenderer.Images.top.bmp", false); }
        }

        /// <summary>
        /// Builder method displaying the geometry in the wpf window.
        /// </summary>
        /// <param name="ctx">Drawing context parameter for the drawing commands.</param>
        public void BuildDrawing(DrawingContext ctx)
        {
            if (ctx != null)
            {
                this.BuildFirstLeftWall(ctx);
                this.BuildFirstRightWall(ctx);

                this.BuildFirstRightFloor(ctx);
                this.BuildFirstLeftFloor(ctx);
                this.BuildFirstLeftTop(ctx);
                this.BuildFirstRightTop(ctx);

                this.BuildSecondRightWall(ctx);
                this.BuildSecondLeftWall(ctx);

                this.BuildSecondRightFloor(ctx);
                this.BuildSecondLeftFloor(ctx);
                this.BuildSecondRightTop(ctx);
                this.BuildSecondLeftTop(ctx);

                this.BuildGround(ctx);
                this.BuildCeiling(ctx);
                this.BuildThirdAisle(ctx);
            }
        }

        private Brush GetBrush(string fname, bool isTiled)
        {
            if (!this.brushes.ContainsKey(fname))
            {
                BitmapImage bmp = new BitmapImage();
                bmp.BeginInit();
                bmp.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream(fname);
                bmp.EndInit();
                ImageBrush ib = new ImageBrush(bmp);

                if (isTiled)
                {
                    ib.TileMode = TileMode.Tile;
                    ib.Viewport = new Rect(0, 0, this.model.GameWidth, this.model.GameHeight);
                    ib.ViewboxUnits = BrushMappingMode.Absolute;
                }

                this.brushes.Add(fname, ib);
            }

            return this.brushes[fname];
        }

        private void BuildCeiling(DrawingContext ctx)
        {
            StreamGeometry streamGeometry = new StreamGeometry();
            using (StreamGeometryContext geometryContext = streamGeometry.Open())
            {
                geometryContext.BeginFigure(new Point(0, 0), true, true);
                PointCollection points = new PointCollection { this.thirdAisle.TopLeft, this.thirdAisle.TopRight, new Point(this.model.GameWidth, 0) };
                geometryContext.PolyLineTo(points, true, true);
            }

            streamGeometry.Freeze();
            ctx.DrawGeometry(this.TopBrush, new Pen(Brushes.Black, 2), streamGeometry);
        }

        private void BuildGround(DrawingContext ctx)
        {
            StreamGeometry streamGeometry = new StreamGeometry();
            using (StreamGeometryContext geometryContext = streamGeometry.Open())
            {
                geometryContext.BeginFigure(new Point(0, this.model.GameHeight), true, true);
                PointCollection points = new PointCollection { this.secondLeftWall.BottomRight, this.secondRightWall.BottomLeft, new Point(this.model.GameWidth, this.model.GameHeight) };
                geometryContext.PolyLineTo(points, true, true);
            }

            streamGeometry.Freeze();
            ctx.DrawGeometry(Brushes.SandyBrown, new Pen(Brushes.Black, 2), streamGeometry);
        }

        private void BuildSecondLeftTop(DrawingContext ctx)
        {
            StreamGeometry streamGeometry = new StreamGeometry();
            using (StreamGeometryContext geometryContext = streamGeometry.Open())
            {
                geometryContext.BeginFigure(this.secondLeftTop1, true, true);
                PointCollection points = new PointCollection { this.secondLeftTop2, this.secondLeftTop3 };
                geometryContext.PolyLineTo(points, true, true);
            }

            streamGeometry.Freeze();
            ctx.DrawGeometry(this.TopBrush, new Pen(Brushes.Black, 2), streamGeometry);
        }

        private void BuildSecondRightTop(DrawingContext ctx)
        {
            StreamGeometry streamGeometry = new StreamGeometry();
            using (StreamGeometryContext geometryContext = streamGeometry.Open())
            {
                geometryContext.BeginFigure(this.secondRightTop1, true, true);
                PointCollection points = new PointCollection { this.secondRightTop2, this.secondRightTop3 };
                geometryContext.PolyLineTo(points, true, true);
            }

            streamGeometry.Freeze();
            ctx.DrawGeometry(this.TopBrush, new Pen(Brushes.Black, 2), streamGeometry);
        }

        private void BuildSecondLeftFloor(DrawingContext ctx)
        {
            StreamGeometry streamGeometry = new StreamGeometry();
            using (StreamGeometryContext geometryContext = streamGeometry.Open())
            {
                geometryContext.BeginFigure(this.secondLeftFloor1, true, true);
                PointCollection points = new PointCollection { this.secondLeftFloor2, this.secondLeftFloor3 };
                geometryContext.PolyLineTo(points, true, true);
            }

            streamGeometry.Freeze();
            ctx.DrawGeometry(this.FloorBrush, new Pen(Brushes.Black, 2), streamGeometry);
        }

        private void BuildSecondRightFloor(DrawingContext ctx)
        {
            StreamGeometry streamGeometry = new StreamGeometry();
            using (StreamGeometryContext geometryContext = streamGeometry.Open())
            {
                geometryContext.BeginFigure(this.secondRightFloor1, true, true);
                PointCollection points = new PointCollection { this.secondRightFloor2, this.secondRightFloor3 };
                geometryContext.PolyLineTo(points, true, true);
            }

            streamGeometry.Freeze();
            ctx.DrawGeometry(this.FloorBrush, new Pen(Brushes.Black, 2), streamGeometry);
        }

        private void BuildFirstRightTop(DrawingContext ctx)
        {
            StreamGeometry streamGeometry = new StreamGeometry();
            using (StreamGeometryContext geometryContext = streamGeometry.Open())
            {
                geometryContext.BeginFigure(this.firstRightTop1, true, true);
                PointCollection points = new PointCollection { this.firstRightTop2, this.firstRightTop3 };
                geometryContext.PolyLineTo(points, true, true);
            }

            streamGeometry.Freeze();
            ctx.DrawGeometry(this.TopBrush, new Pen(Brushes.Black, 2), streamGeometry);
        }

        private void BuildFirstLeftTop(DrawingContext ctx)
        {
            StreamGeometry streamGeometry = new StreamGeometry();
            using (StreamGeometryContext geometryContext = streamGeometry.Open())
            {
                geometryContext.BeginFigure(this.firstLeftTop1, true, true);
                PointCollection points = new PointCollection { this.firstLeftTop2, this.firstLeftTop3 };
                geometryContext.PolyLineTo(points, true, true);
            }

            streamGeometry.Freeze();
            ctx.DrawGeometry(this.TopBrush, new Pen(Brushes.Black, 2), streamGeometry);
        }

        private void BuildFirstLeftFloor(DrawingContext ctx)
        {
            StreamGeometry streamGeometry = new StreamGeometry();
            using (StreamGeometryContext geometryContext = streamGeometry.Open())
            {
                geometryContext.BeginFigure(this.firstLeftFloor1, true, true);
                PointCollection points = new PointCollection { this.firstLeftFloor2, this.firstLeftFloor3 };
                geometryContext.PolyLineTo(points, true, true);
            }

            streamGeometry.Freeze();
            ctx.DrawGeometry(this.FloorBrush, new Pen(Brushes.Black, 2), streamGeometry);
        }

        private void BuildFirstRightFloor(DrawingContext ctx)
        {
            StreamGeometry streamGeometry = new StreamGeometry();
            using (StreamGeometryContext geometryContext = streamGeometry.Open())
            {
                geometryContext.BeginFigure(this.firstRightFloor1, true, true);
                PointCollection points = new PointCollection { this.firstRightFloor2, this.firstRightFloor3 };
                geometryContext.PolyLineTo(points, true, true);
            }

            streamGeometry.Freeze();
            ctx.DrawGeometry(this.FloorBrush, new Pen(Brushes.Black, 2), streamGeometry);
        }

        private void BuildSecondLeftWall(DrawingContext ctx)
        {
            ctx.DrawRectangle(this.WallBrush, this.wallPen, this.secondLeftWall);
        }

        private void BuildSecondRightWall(DrawingContext ctx)
        {
            ctx.DrawRectangle(this.WallBrush, this.wallPen, this.secondRightWall);
        }

        private void BuildFirstLeftWall(DrawingContext ctx)
        {
            ctx.DrawRectangle(this.WallBrush, this.wallPen, this.firstLeftWall);
        }

        private void BuildFirstRightWall(DrawingContext ctx)
        {
            ctx.DrawRectangle(this.WallBrush, this.wallPen, this.firstRightWall);
        }

        private void BuildThirdAisle(DrawingContext ctx)
        {
            ctx.DrawRectangle(Brushes.DimGray, this.wallPen, this.thirdAisle);
        }
    }
}
