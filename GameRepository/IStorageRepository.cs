﻿namespace GameRepository
{
    using System;
    using System.Collections.Generic;
    using GameModel;

    /// <summary>
    /// Defines the repository of the game.
    /// </summary>
    public interface IStorageRepository
    {
        /// <summary>
        /// Saves the game.
        /// </summary>
        /// <param name="elapsedTime">The actual run time of the game.</param>
        /// <param name="path">The actual game state saved here.</param>
        void SaveGame(TimeSpan elapsedTime, string path);

        /// <summary>
        /// Loads the game.
        /// </summary>
        /// <param name="path">Path pointing to the file that contains the proper game model description.</param>
        /// <returns>The GameModel filled with data.</returns>
        IGameModel LoadGame(string path);

        /// <summary>
        /// Saves the highscore.
        /// </summary>
        /// <param name="completionTime">The completion time of the level.</param>
        /// <param name="savename">The players name for identifying their highscore.</param>
        void SaveHighscore(TimeSpan completionTime, string savename);

        /// <summary>
        /// Gets the highscore collection from the data storage.
        /// </summary>
        /// <returns>The collection that contains the highscores.</returns>
        ICollection<Highscore> LoadHighscore();
    }
}
