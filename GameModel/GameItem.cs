﻿namespace GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.Contracts;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// GameItem class.
    /// </summary>
    public abstract class GameItem
    {
        /// <summary>
        /// Gets or sets x coordinate of the item.
        /// </summary>
        public int X { get; set; }

        /// <summary>
        /// Gets or sets y coordinate of the item.
        /// </summary>
        public int Y { get; set; }

        /// <summary>
        /// Checks if two items are on the same coordinate.
        /// </summary>
        /// <param name="otherItem">The other interactable item.</param>
        /// <returns>Returns if the two items are on the same coordinate.</returns>
        public bool Interact(GameItem otherItem)
        {
            Contract.Requires(otherItem != null);
            return this.X == otherItem.X && this.Y == otherItem.Y;
        }
    }
}
