﻿namespace GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The key placed on the map to collect.
    /// </summary>
    public class KeyToCollect : GameItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="KeyToCollect"/> class.
        /// </summary>
        /// <param name="coordX">X coordinate of the key.</param>
        /// <param name="coordY">Y coordinate of the key.</param>
        public KeyToCollect(int coordX, int coordY)
        {
            this.X = coordX;
            this.Y = coordY;
        }

        /// <summary>
        /// Proper unit testing needed overwritten Equals(object obj) method.
        /// </summary>
        /// <param name="obj">An object that is compared to the KeyToCollect object.</param>
        /// <returns>The boolean value whether the parameter is equal with the KeyToCollect object.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is KeyToCollect))
            {
                return false;
            }

            var param = obj as KeyToCollect;
            return param.X == this.X && param.Y == this.Y;
        }

        /// <summary>
        /// Proper unit testing needed overridden GetHashCode() method.
        /// </summary>
        /// <returns>The default hash.</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
