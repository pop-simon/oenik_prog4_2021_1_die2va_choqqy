﻿namespace GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Enum class of the direction of the player.
    /// </summary>
    public enum Direction
    {
        /// <summary>
        /// North direction.
        /// </summary>
        North,

        /// <summary>
        /// East direction.
        /// </summary>
        East,

        /// <summary>
        /// South direction.
        /// </summary>
        South,

        /// <summary>
        ///  West direction.
        /// </summary>
        West,
    }

    /// <summary>
    /// The player class with two coordinates and a facing direction.
    /// </summary>
    public class Player : GameItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// </summary>
        /// <param name="coordX">X coordinate of the player.</param>
        /// <param name="coordY">Y coordinate of the player.</param>
        /// <param name="dir">Direction of the player.</param>
        public Player(int coordX, int coordY, Direction dir)
        {
            this.X = coordX;
            this.Y = coordY;
            this.ActualDirection = dir;
            this.Inventory = 0;
        }

        /// <summary>
        /// Gets or sets inventory of the player, storing the number of keys.
        /// </summary>
        public int Inventory { get; set; }

        /// <summary>
        /// Gets or sets the direction of the player facing.
        /// </summary>
        public Direction ActualDirection { get; set; }

        /// <summary>
        /// Proper unit testing needed overwritten Equals(object obj) method.
        /// </summary>
        /// <param name="obj">An object that is compared to the Player.</param>
        /// <returns>The boolean value whether the parameter is equal with the Player object.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is Player))
            {
                return false;
            }

            var param = obj as Player;
            return param.X == this.X && param.Y == this.Y && param.ActualDirection == this.ActualDirection && param.Inventory == this.Inventory;
        }

        /// <summary>
        /// Proper unit testing needed overridden GetHashCode() method.
        /// </summary>
        /// <returns>The default hash.</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Proper unit testing needed overridden ToString() method.
        /// </summary>
        /// <returns>The string that describes the player object.</returns>
        public override string ToString()
        {
            return $"Player({this.X},{this.Y},{this.ActualDirection.ToString()})";
        }
    }
}
