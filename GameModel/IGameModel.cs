﻿namespace GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the game model.
    /// </summary>
    public interface IGameModel
    {
        /// <summary>
        /// Gets or sets the player.
        /// </summary>
        public Player ThePlayer { get; set; }

        /// <summary>
        /// Gets or sets the exit of the dungeon.
        /// </summary>
        public Door Door { get; set; }

        /// <summary>
        /// Gets or sets the collection of every keys on the map.
        /// </summary>
        public ICollection<KeyToCollect> Keys { get; set; }

        /// <summary>
        /// Gets or sets the walls as an array. If Walls[x,y]==true it means there is a wall item on the (x,y) coordinates.
        /// </summary>
        public bool[,] Walls { get; set; }

        /// <summary>
        /// Gets or sets the number of key(s) that need to collect to open the door.
        /// </summary>
        public int NumberOfKeysToOpenDoor { get; set; }

        /// <summary>
        /// Gets or sets the sum of the running time.
        /// </summary>
        public TimeSpan ElapsedTime { get; set; }

        /// <summary>
        /// Gets the host windows height.
        /// </summary>
        public double GameWidth { get; }

        /// <summary>
        /// Gets the host windows width.
        /// </summary>
        public double GameHeight { get; }
    }
}