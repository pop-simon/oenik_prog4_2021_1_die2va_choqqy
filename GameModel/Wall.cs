﻿namespace GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Wall game item class.
    /// </summary>
    public class Wall : GameItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Wall"/> class.
        /// </summary>
        /// <param name="coordX">X coordinate of the wall.</param>
        /// <param name="coordY">Y coordinate of the wall.</param>
        public Wall(int coordX, int coordY)
        {
            this.X = coordX;
            this.Y = coordY;
        }
    }
}
