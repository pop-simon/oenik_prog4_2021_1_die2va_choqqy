﻿namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using GameModel;

    /// <summary>
    /// Defines the logic of the game.
    /// </summary>
    public interface IGameLogic
    {
        /// <summary>
        /// Event that is raised if the player collected enough keys to open the door.
        /// </summary>
        event EventHandler DoorOpenedEvent;

        /// <summary>
        /// Event that is raised when the player tries to enter the closed door.
        /// </summary>
        event EventHandler EnteringClosedDoorEvent;

        /// <summary>
        /// Event that is raised when the player collects a key.
        /// </summary>
        event EventHandler KeyCollectedEvent;

        /// <summary>
        /// Method that opens the door if the player collected enough keys.
        /// </summary>
        void TryOpenTheDoor();

        /// <summary>
        /// Method that collects a key.
        /// </summary>
        void TryCollectKey();

        /// <summary>
        /// Rotates the player around its own axis.
        /// </summary>
        /// <param name="direction">If it is 1, then the player rotates right, if it is -1, then rotates left.</param>
        void Rotate(int direction);

        /// <summary>
        /// Moves forward to the players actual direction.
        /// </summary>
        /// <returns>Whether the player reached the end of the dungeon.</returns>
        bool Move();

        /// <summary>
        /// Initializes the game model that will be used.
        /// </summary>
        /// <param name="path">Path pointing to the file that contains the proper game model description.</param>
        void InitializeModel(string path);

        /// <summary>
        /// Saves the game.
        /// </summary>
        /// <param name="elapsedTime">The actual run time of the game.</param>
        /// <param name="path">The actual game state saved here.</param>
        void SaveGame(TimeSpan elapsedTime, string path);

        /// <summary>
        /// Loads the game.
        /// </summary>
        /// <param name="path">Path pointing to the file that contains the proper game model description.</param>
        /// <returns>An IGameModel object that contains the loaded data.</returns>
        IGameModel LoadGame(string path);

        /// <summary>
        /// Saves the new highscore.
        /// </summary>
        /// <param name="completionTime">The completion time of the level.</param>
        /// <param name="savename">The players name for identifying their highscore.</param>
        void SaveHighscore(TimeSpan completionTime, string savename);

        /// <summary>
        /// Gets the highscore collection from the data storage.
        /// </summary>
        /// <returns>The collection that contains the highscores.</returns>
        ICollection<Highscore> LoadHighscore();
    }
}
