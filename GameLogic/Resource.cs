﻿namespace GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class that manages the resource loading procedure.
    /// </summary>
    public class Resource : IResource
    {
        /// <summary>
        /// Loads the needed level from the exe file and converts it into a string array.
        /// </summary>
        /// <param name="filename">The embedded resources route.</param>
        /// <returns>A string array that contains the loaded level.</returns>
        public string[] LoadLevel(string filename)
        {
            Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(filename);
            StreamReader streamReader = new StreamReader(stream);
            string[] lines = streamReader.ReadToEnd().Replace("\r", string.Empty, StringComparison.CurrentCultureIgnoreCase).Split('\n');
            streamReader.Close();
            return lines;
        }
    }
}
