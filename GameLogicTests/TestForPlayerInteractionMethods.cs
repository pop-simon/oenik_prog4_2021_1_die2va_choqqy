﻿namespace GameLogicTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameLogic;
    using GameModel;
    using GameRepository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Class for testing the GameLogics Rotate(int direction) and Move() methods.
    /// </summary>
    [TestFixture]
    internal class TestForPlayerInteractionMethods
    {
        private Mock<IGameModel> mockedModel;
        private Mock<IStorageRepository> mockedStorageRepository;
        private Mock<IResource> mockedResource;
        private Player playerToRotate;
        private Player playerToMoveSouth;
        private Player playerToEnterDoor;

        /// <summary>
        /// Test for rotating the player clockwise.
        /// </summary>
        [Test]
        public void TestForPlayerRotationClockwise()
        {
            var logic = this.CreateLogicWithMocksForRotateTest();

            logic.Rotate(1);

            Assert.That(this.playerToRotate.ActualDirection, Is.EqualTo(Direction.North));
        }

        /// <summary>
        /// Test for rotating the player counter clockwise.
        /// </summary>
        [Test]
        public void TestForPlayerRotationCounterClockwise()
        {
            var logic = this.CreateLogicWithMocksForRotateTest();

            logic.Rotate(-1);

            Assert.That(this.playerToRotate.ActualDirection, Is.EqualTo(Direction.South));
        }

        /// <summary>
        /// Test for false parameter to the Rotate(int direction) method.
        /// </summary>
        [Test]
        public void TestForPlayerRotationWithFalseParameter()
        {
            var logic = this.CreateLogicWithMocksForRotateTest();

            logic.Rotate(It.IsAny<int>());

            Assert.That(this.playerToRotate.ActualDirection, Is.EqualTo(Direction.West));
        }

        /// <summary>
        /// Test for player movement to southern direction.
        /// </summary>
        [Test]
        public void TestForPlayerMovement()
        {
            var logic = this.CreateLogicWithMocksForMoveSouth();

            var result = logic.Move();

            Assert.That(result, Is.False);
            Assert.That(this.playerToMoveSouth, Is.EqualTo(new Player(1, 2, Direction.South)));
        }

        /// <summary>
        /// Test for player enter the door.
        /// </summary>
        [Test]
        public void TestForPlayerToEnterDoor()
        {
            var logic = this.CreateLogicWithMocksForEnterDoor();

            var result = logic.Move();

            Assert.That(result, Is.True);
            Assert.That(this.playerToEnterDoor, Is.EqualTo(new Player(2, 2, Direction.East)));
        }

        private Logic CreateLogicWithMocksForEnterDoor()
        {
            this.mockedModel = new Mock<IGameModel>();
            this.mockedResource = new Mock<IResource>();
            this.mockedStorageRepository = new Mock<IStorageRepository>();

            this.playerToEnterDoor = new Player(1, 2, Direction.East);
            bool[,] walls = new bool[4, 4];
            for (int i = 0; i < walls.GetLength(0); i++)
            {
                for (int j = 0; j < walls.GetLength(1); j++)
                {
                    if (i % 3 == 0 || j % 3 == 0)
                    {
                        walls[i, j] = true;
                    }
                }
            }

            walls[1, 1] = false;
            walls[1, 2] = false;
            walls[2, 1] = false;
            walls[2, 2] = false;

            Door door = new Door(2, 2) { Closed = false };

            this.mockedModel.Setup(model => model.ThePlayer).Returns(this.playerToEnterDoor);
            this.mockedModel.Setup(model => model.Walls).Returns(walls);
            this.mockedModel.Setup(model => model.Door).Returns(door);

            return new Logic(this.mockedModel.Object, this.mockedStorageRepository.Object, this.mockedResource.Object);
        }

        private Logic CreateLogicWithMocksForMoveSouth()
        {
            this.mockedModel = new Mock<IGameModel>();
            this.mockedResource = new Mock<IResource>();
            this.mockedStorageRepository = new Mock<IStorageRepository>();

            this.playerToMoveSouth = new Player(1, 1, Direction.South);
            bool[,] walls = new bool[4, 4];
            for (int i = 0; i < walls.GetLength(0); i++)
            {
                for (int j = 0; j < walls.GetLength(1); j++)
                {
                    if (i % 3 == 0 || j % 3 == 0)
                    {
                        walls[i, j] = true;
                    }
                }
            }

            walls[1, 1] = false;
            walls[1, 2] = false;
            walls[2, 1] = false;
            walls[2, 2] = false;

            Door door = new Door(2, 2);

            this.mockedModel.Setup(model => model.ThePlayer).Returns(this.playerToMoveSouth);
            this.mockedModel.Setup(model => model.Walls).Returns(walls);
            this.mockedModel.Setup(model => model.Door).Returns(door);

            return new Logic(this.mockedModel.Object, this.mockedStorageRepository.Object, this.mockedResource.Object);
        }

        private Logic CreateLogicWithMocksForRotateTest()
        {
            this.mockedModel = new Mock<IGameModel>();
            this.mockedResource = new Mock<IResource>();
            this.mockedStorageRepository = new Mock<IStorageRepository>();

            this.playerToRotate = new Player(1, 1, Direction.West);

            this.mockedModel.Setup(model => model.ThePlayer).Returns(this.playerToRotate);

            return new Logic(this.mockedModel.Object, this.mockedStorageRepository.Object, this.mockedResource.Object);
        }
    }
}
