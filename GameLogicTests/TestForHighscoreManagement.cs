﻿namespace GameLogicTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameLogic;
    using GameModel;
    using GameRepository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Class for testing the GameLogics LoadHighscore and SaveHighscore method.
    /// </summary>
    [TestFixture]
    internal class TestForHighscoreManagement
    {
        private Mock<IGameModel> mockedModel;
        private Mock<IStorageRepository> mockedStorageRepository;
        private Mock<IResource> mockedResource;
        private ICollection<Highscore> expectedHighscores;

        /// <summary>
        /// Test for gamelogics SaveHighscore method.
        /// </summary>
        [Test]
        public void TestForSaveHighscore()
        {
            var logic = this.CreateGameLogicForHighscoreTests();

            logic.SaveHighscore(It.IsAny<TimeSpan>(), It.IsAny<string>());

            this.mockedStorageRepository.Verify(srepo => srepo.SaveHighscore(It.IsAny<TimeSpan>(), It.IsAny<string>()), Times.Once);
            this.mockedStorageRepository.Verify(srepo => srepo.LoadHighscore(), Times.Never);
        }

        /// <summary>
        /// Test for gamelogics LoadHighscore method.
        /// </summary>
        [Test]
        public void TestForLoadHighscore()
        {
            var logic = this.CreateGameLogicForHighscoreTests();

            var result = logic.LoadHighscore();

            Assert.That(result, Is.EqualTo(this.expectedHighscores));
            this.mockedStorageRepository.Verify(srepo => srepo.LoadHighscore(), Times.Once);
            this.mockedStorageRepository.Verify(srepo => srepo.SaveHighscore(It.IsAny<TimeSpan>(), It.IsAny<string>()), Times.Never);
        }

        private Logic CreateGameLogicForHighscoreTests()
        {
            this.mockedModel = new Mock<IGameModel>();
            this.mockedStorageRepository = new Mock<IStorageRepository>();
            this.mockedResource = new Mock<IResource>();

            ICollection<Highscore> highscores = new List<Highscore>();
            highscores.Add(new Highscore()
            {
                ID = "ixc9-cnZ00_hEky3h:Ht5dr",
                Name = "John Doe",
                Level = "12",
                ElapsedTime = TimeSpan.FromSeconds(324),
            });

            this.expectedHighscores = new List<Highscore>();
            this.expectedHighscores.Add(new Highscore()
            {
                ID = "ixc9-cnZ00_hEky3h:Ht5dr",
                Name = "John Doe",
                Level = "12",
                ElapsedTime = TimeSpan.FromSeconds(324),
            });

            this.mockedStorageRepository.Setup(srepo => srepo.LoadHighscore()).Returns(highscores);
            this.mockedStorageRepository.Setup(srepo => srepo.SaveHighscore(It.IsAny<TimeSpan>(), It.IsAny<string>())).Verifiable();

            return new Logic(this.mockedModel.Object, this.mockedStorageRepository.Object, this.mockedResource.Object);
        }
    }
}
