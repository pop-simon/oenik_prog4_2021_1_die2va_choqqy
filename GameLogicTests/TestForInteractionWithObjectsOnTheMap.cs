﻿namespace GameLogicTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameLogic;
    using GameModel;
    using GameRepository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Class for testing the GameLogics TryCollectKey() and TryOpenTheDoor() methods.
    /// </summary>
    [TestFixture]
    internal class TestForInteractionWithObjectsOnTheMap
    {
        private Mock<IGameModel> mockedModel;
        private Mock<IStorageRepository> mockedStorageRepo;
        private Mock<IResource> mockedResource;
        private Door openingDoor;
        private ICollection<KeyToCollect> keysToCollect;
        private Player playerThatCollectsKey;

        /// <summary>
        /// Test for gamelogics TryOpenTheDoor method.
        /// </summary>
        [Test]
        public void TestTryOpenTheDoor()
        {
            var logic = this.CreateLogicWithMocksForDoorTest();

            logic.TryOpenTheDoor();

            Assert.That(this.openingDoor.Closed, Is.False);
        }

        /// <summary>
        /// Test for gamelogics TestTryCollectKey method.
        /// </summary>
        [Test]
        public void TestTryCollectKey()
        {
            var logic = this.CreateLogicWithMocksForKeysTest();

            logic.TryCollectKey();

            Assert.That(this.keysToCollect, Is.Empty);
            Assert.That(this.playerThatCollectsKey.Inventory, Is.EqualTo(1));
        }

        private Logic CreateLogicWithMocksForKeysTest()
        {
            this.mockedModel = new Mock<IGameModel>();
            this.mockedResource = new Mock<IResource>();
            this.mockedStorageRepo = new Mock<IStorageRepository>();

            this.keysToCollect = new List<KeyToCollect>()
            {
                new KeyToCollect(1, 2),
            };

            this.playerThatCollectsKey = new Player(1, 2, Direction.East);

            this.mockedModel.Setup(model => model.Keys).Returns(this.keysToCollect);
            this.mockedModel.Setup(model => model.ThePlayer).Returns(this.playerThatCollectsKey);

            return new Logic(this.mockedModel.Object, this.mockedStorageRepo.Object, this.mockedResource.Object);
        }

        private Logic CreateLogicWithMocksForDoorTest()
        {
            this.mockedModel = new Mock<IGameModel>();
            this.mockedResource = new Mock<IResource>();
            this.mockedStorageRepo = new Mock<IStorageRepository>();

            Player player = new Player(1, 2, Direction.East)
            {
                Inventory = 1,
            };

            this.openingDoor = new Door(2, 2)
            {
                Closed = true,
            };

            this.mockedModel.Setup(model => model.ThePlayer).Returns(player);
            this.mockedModel.Setup(model => model.Door).Returns(this.openingDoor);
            this.mockedModel.Setup(model => model.NumberOfKeysToOpenDoor).Returns(1);

            return new Logic(this.mockedModel.Object, this.mockedStorageRepo.Object, this.mockedResource.Object);
        }
    }
}
